/*global console, module, require*/
module.exports = function (grunt) {
  // Load multiple grunt tasks using globbing patterns
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Run MarkdownLint
    markdownlint: {
      full: {
        options: {
          config: {
            'default': true,
          }
        },
        src: ['**/*.md', '!out/**/*.md', '!node_modules/**/*.md', '!vendor/**/*.md'],
      }
    },

    // Run PHPCS and php -l
    exec: {
      phpsyntax: {
        cmd: 'find -L . -not -path "./vendor/*" -name "*.php" -print0 | xargs -0 -n 1 -P 4 php -l',
      },
      phpcs: {
        cmd:
          './vendor/bin/phpcs . -p --standard=WordPress,WordPress-VIP-Go --extensions=php --warning-severity=8 --error-severity=1 --ignore=vendor,node_modules',
      },
    },
  });

  // Build task(s).
  grunt.registerTask('lint_markdownlint', ['markdownlint']);
  grunt.registerTask('standards_phpsyntax', ['exec:phpsyntax']);
  grunt.registerTask('standards_phpcs', ['exec:phpcs']);

  grunt.registerTask('test', [
    'lint_markdownlint',
    'standards_phpsyntax',
    'standards_phpcs',
  ]);
};
