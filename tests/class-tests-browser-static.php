<?php
/**
 * Static browser user tests
 *
 * @package     Browser\Tests\Browser\Static
 * @since       3.0.0
 */

declare( strict_types = 1 );

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use PHPUnit\Framework\TestCase;

require_once dirname( __FILE__ ) . '/class-tab-delimited-file-iterator.php';

/**
 * Static browser unit tests
 *
 * @since       3.0.0
 */
final class Tests_Browser_Static extends TestCase {


	/**
	 * Test static providers
	 *
	 * @access      public
	 * @since       3.0.0
	 * @param       string $user_agent The user agent to test.
	 * @param       string $browser_type The type of browser.
	 * @param       string $browser_name The name of the browser.
	 * @param       string $browser_version The version of the browser.
	 * @param       string $os_type The type of operating system associated with the browser.
	 * @param       string $os_name The name of the operating system associated with the browser.
	 * @param       string $os_version_name The version of the operating system.
	 * @param       string $os_version_number The version of the operating system.
	 * @return      void
	 */
	public function test_static_user_agents( $user_agent, $browser_type, $browser_name, $browser_version, $os_type, $os_name, $os_version_name, $os_version_number ) {
		$browser = new Browser( $user_agent );

		$this->assertSame( (string) $browser_name, $browser->get_browser() );
		$this->assertSame( (string) $version, $browser->get_version() );
		$this->assertSame( (string) $platform, $browser->get_platform() );
		$this->assertSame( (bool) $mobile, $browser->is_mobile() );
	}


	/**
	 * Get the static provider data
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      array The static provider data
	 */
	public function user_agent_static_provider() {
		return array(
			array(
				'useragent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
				'browser'   => Browser::BROWSER_IE,
				'version'   => '11.0',
				'platform'  => Browser::PLATFORM_WINDOWS,
				'mobile'    => false,
			),
			array(
				'useragent' => 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0',
				'browser'   => Browser::BROWSER_FIREFOX,
				'version'   => '41.0',
				'platform'  => Browser::PLATFORM_ANDROID,
				'mobile'    => true,
			),
			array(
				'useragent' => 'Mozilla/5.0 (Android 4.4; Tablet; rv:41.0) Gecko/41.0 Firefox/41.0',
				'browser'   => Browser::BROWSER_FIREFOX,
				'version'   => '41.0',
				'platform'  => Browser::PLATFORM_ANDROID,
				'mobile'    => true,
			),
			array(
				'useragent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) CriOS/28.0.1500.16 Mobile/10B350 Safari/8536.25',
				'browser'   => Browser::BROWSER_CHROME,
				'version'   => '28.0.1500.16',
				'platform'  => Browser::PLATFORM_IPHONE,
				'mobile'    => true,
			),
			array(
				'useragent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.11 (KHTML, like Gecko) DumpRenderTree/0.0.0.0 Safari/536.11',
				'browser'   => Browser::BROWSER_SAFARI,
				'version'   => 'unknown', // All we really know here is that it's based on webkit, no version information is provided.
				'platform'  => Browser::PLATFORM_LINUX,
				'mobile'    => false,
			),
			array(
				'useragent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) CriOS/28.0.1500.16 Mobile/10B142 Safari/8536.25',
				'browser'   => Browser::BROWSER_CHROME,
				'version'   => '28.0.1500.16',
				'platform'  => Browser::PLATFORM_IPHONE,
				'mobile'    => true,
			),
			array(
				'useragent' => 'Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0; HTC; Radar; Orange)',
				'browser'   => Browser::BROWSER_POCKET_IE,
				'version'   => '9.0',
				'platform'  => Browser::PLATFORM_WINDOWS,
				'mobile'    => true,
			),
		);
	}
}
