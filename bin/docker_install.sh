!/usr/bin/env bash

# We need to install dependencies only for Docker
[ ! -e /.dockerenv ] && exit 0

set -xe

sudo apt-get clean
sudo apt-get -yqq update
